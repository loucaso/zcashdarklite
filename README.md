ZCASHDARKLITECOIN
==========================

ZCASHDARKLITE development tree

Z cash dark lite is a scrypt PoS-based cryptocurrency.

Development process


https://www.zcashdarklite.online


MULTI POOL BR
==========================

https://www.multipoolbr.online


BlockExplorer
=================

http://explorer.zcashdarklite.online


Coin Spec
===========================


COIN = ZCASHDARKLITE (ZCDL)


ALGO = SCRYPT 


TIPO = PoS + PoW


TOTAL POW SUPPLY = 3 000 000


MAX SUPPLY = 88 000 000 


BLOCK TIME = 1 MIN 


MIN MATURE TIME = 8 HOURS 


TRANSACTION CONFIRMATIONS = 5 BLOCKS 


MATURE BLOCKS = 10 BLOCKS 


PREMINE = 2% 


PAYTXFEE = 0.001 


PORT RCP = 21565 


PROOF OF STAKE (POS):
=====================

32001 - 40000 = 5.55Dias - 1800% 


40000 - 40320 = 5.33Hrs - 1000% 


40320 - 79200 = 27Dias - 500%


79200 - 83520 = 3D - 3000% 


83520 - 84960 = 1D - 6000% (SUPER BLOCO)


84960 - 95040 = 7D - 1200%


95040 - 115200 = 14D - 1800%


115200 - 115520 = 5.33H - 1000%


115520 - 154400 = 27D - 500%


154400 - 158720 = 3D - 3000% 


158720 - 160160 = 1D - 6000% (SUPER BLOCO)


160160 - 170240 = 4D - 1200%


170240 - 190400 = 14D - 1800%


190400 - 


Nós Oficiais
================


s1.zcashdarklite.online UNITED STATES 


s2.zcashdarklite.online BRASIL/RJ 


s3.zcashdarklite.online GERMANY 



Wallets
=================

Linux-QT:


NOT AVAILABLE


Windows:


NOT AVAILABLE


Install Required Packages
=======================


apt-get install build-essential libtool autotools-dev autoconf pkg-config libssl-dev -y


apt-get install libboost-all-dev git npm nodejs nodejs-legacy libminiupnpc-dev redis-server -y 


add-apt-repository ppa:bitcoin/bitcoin


apt-get update


apt-get install libdb4.8-dev libdb4.8++-dev -y


apt-get install git -y


cd

git clone https://github.com/loucaso/Zcashdarklite.git


cd Zcashdarklite/src


make -f makefile.unix USE_UPNP=- USE_QRCODE=- -j1


Start Daemon

./zcashdarklited


EPOCH TIME FINAL FORK
===============

1520267003 


Monday, 5 de March de 2018 às 16:23:23


Segunda-feira, 5 de março de 2018 às 13:23:23 GMT-03:00


# Zcashdarklite 2017-2018
